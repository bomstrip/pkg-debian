#!/bin/sh
#
# Written by Peter Pentchev in 2008.
# This file is hereby placed into the public domain.

set -e

[ -z "$BOMSTRIP" ] && BOMSTRIP='bomstrip'

if [ "$#" -eq 0 ]; then
	echo 'Usage: bomstrip-files file...' 1>&2
	exit 1
fi

while [ "$#" -ne 0 ]; do
	cp "$1" "$1.bom"
	$BOMSTRIP < "$1.bom" > "$1"
	shift
done
